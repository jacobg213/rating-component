/// <reference types="vitest" />
/// <reference types="@vitest/browser/providers/webdriverio" />

import { defineConfig } from 'vite'

export default defineConfig({
  test: {
    typecheck: {
        tsconfig: './src/tests/tsconfig.ts'
    },
    // Following the advice from lit docs we will be testing in the browser - https://lit.dev/docs/v2/tools/testing/#testing-in-the-browser
    // TODO: Should test in more browsers than just Chrome - https://vitest.dev/guide/browser#browser-option-types
    browser: {
      enabled: true,
      headless: true,
      name: 'chrome',
      provider: 'webdriverio',
    },
  },
})
