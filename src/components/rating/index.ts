import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { createRef, Ref, ref } from 'lit/directives/ref.js';
import style from './style.css?raw';

export enum StarSize {
  S = 1,
  M = 2,
  L = 3
}

export type RatingChangedEventPayload = {
  value: number;
};

export type RatingChangedEvent = CustomEvent<RatingChangedEventPayload>;

@customElement(`case-rating`)
export class CaseRating extends LitElement {
  render() {
    return html`
      <style>${style}</style>

      <!--
        Firefox requires Nightly for popover support.
        By default this feature will only work in Chromium based browsers and Safari (although the positioning is off on Safari and I do not have an Apple device at home to play with).
        A polyfill is also available at https://github.com/oddbird/popover-polyfill.
        It's quite easy to achieve the same effect differently but this is cooler. :D
      -->
      <span
        ${ref(this.#popOverRef)}
        popover="manual"
        anchor="rating"
        aria-hidden="true"
        tabindex="-1"
        class="rating-popover">${this.value} / 5</span>

      <input
        ${ref(this.#inputRef)}
        aria-label="Rating"
        id="rating"
        name="rating"
        class="rating"
        max="5"
        step="0.5"
        type="range"
        style="--value: ${this.value}; --starsize: ${this.size}rem;"
        value="${this.value}"
        ?disabled=${this.disabled}
        @input="${this.#onInput}">
    `;
  }

  @property()
  value = 0;

  @property()
  size = StarSize.M;

  @property()
  popOverVisibilityTime = 2000;

  @property()
  disabled?: boolean;

  #inputRef: Ref<HTMLInputElement> = createRef();
  #popOverRef: Ref<HTMLSpanElement> = createRef();
  #popOverTimeout: number;

  public showValuePopOver(autoHide = true) {
    const inputRect = this.#inputRef.value?.getBoundingClientRect();
    if (!inputRect) return;


    const integerValue = Math.ceil(this.value);
    const padding = 8;
    const starCount = 5;
    const inputWidthWithoutMarginAndPadding = inputRect.width - padding;

    const valuePopOver = this.#popOverRef.value;
    if (!valuePopOver) return;

    /*
     * Calculate where to display the popover.
     * integerValue * 2 - 1 = the center point of the star for current value when value is > 1.
     */
    const currentStarCenter = integerValue < 1
      ? inputWidthWithoutMarginAndPadding / (starCount * 2) + inputRect.left + padding / 2
      : inputWidthWithoutMarginAndPadding / (starCount * 2) * (integerValue * 2 - 1) + inputRect.left + padding / 2;

    const popOverOffsetTop = 45;
    valuePopOver.style.setProperty('top', `${inputRect.top - popOverOffsetTop}px`);
    valuePopOver.style.setProperty('left', `${currentStarCenter}px`);
    valuePopOver.showPopover();

    if (this.#popOverTimeout) {
      clearTimeout(this.#popOverTimeout);
    }

    if (!autoHide) return;

    this.#popOverTimeout = window.setTimeout(() => this.hideValuePopOver(), this.popOverVisibilityTime);
  }

  public hideValuePopOver() {
    this.#popOverRef.value?.hidePopover();
  }

  #onInput() {
    const inputElement = this.#inputRef.value!;

    // Round the value to make sure we're setting the rating in correct 0.5 steps
    this.value = Math.round(inputElement.valueAsNumber * 2) / 2;

    this.setAttribute('value', this.value.toString());
    inputElement.style.setProperty('--value', this.value.toString());

    this.dispatchEvent(
      new CustomEvent<RatingChangedEventPayload>('rating-changed', {
        detail: {
          value: this.value
        }
      })
    );

    this.showValuePopOver();
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'case-rating': CaseRating;
  }
}
