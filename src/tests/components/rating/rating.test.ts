import { beforeEach, describe, expect, it } from 'vitest'
// TODO: Should set up path aliases
import '../../../components/rating/index';
import { RatingChangedEvent } from '../../../components/rating/index';

function getRatingComponent() {
    return document.body.querySelector('case-rating')!;
}

function getRatingComponentHTML() {
    return getRatingComponent().shadowRoot!;
}

describe('Rating component', () => {
    beforeEach(() => {
        document.body.innerHTML = '<case-rating></case-rating>'
      });

    it('should render', () => {
        const componentHtml = getRatingComponentHTML();
        expect(componentHtml.querySelector('input')?.value).toBe('0');
        expect(componentHtml.querySelector('span')?.textContent).toBe('0 / 5');
    });

    it('should be possible to set rating by updating the range input', async () => {
        const component = getRatingComponent();
        const componentHtml = getRatingComponentHTML();
        const ratingInput = componentHtml.querySelector('input')!;

        const inputEvent = new Event('input');
        ratingInput.value = '3.5';
        ratingInput.dispatchEvent(inputEvent);
        await component.updateComplete;

        expect(componentHtml.querySelector('input')?.value).toBe('3.5');
        expect(componentHtml.querySelector('span')?.textContent).toBe('3.5 / 5');
    });

    it('should round rating to nearest 0.5', async () => {
        const component = getRatingComponent();
        const componentHtml = getRatingComponentHTML();
        const ratingInput = componentHtml.querySelector('input')!;

        const inputEvent = new Event('input');
        ratingInput.value = '3.3';
        ratingInput.dispatchEvent(inputEvent);
        await component.updateComplete;

        expect(componentHtml.querySelector('input')?.value).toBe('3.5');
        expect(componentHtml.querySelector('span')?.textContent).toBe('3.5 / 5');
    });

    it('should show popover on value update', async () => {
        const component = getRatingComponent();
        const componentHtml = getRatingComponentHTML();
        const ratingInput = componentHtml.querySelector('input')!;

        // Expect the popover to not be open at first
        expect(componentHtml.querySelector('span:popover-open')).toBe(null);

        const inputEvent = new Event('input');
        ratingInput.value = '3.5';
        ratingInput.dispatchEvent(inputEvent);
        await component.updateComplete;

        expect(componentHtml.querySelector('span:popover-open')).not.toBe(null);
    });

    it('should dispatch rating changed event on value update', async () => {
        const component = getRatingComponent();
        const componentHtml = getRatingComponentHTML();
        const ratingInput = componentHtml.querySelector('input')!;

        let value: number = 0;
        component.addEventListener('rating-changed', (event: RatingChangedEvent) => {
            value = event.detail.value;
        });

        const inputEvent = new Event('input');
        ratingInput.value = '4';
        ratingInput.dispatchEvent(inputEvent);
        await component.updateComplete;

        expect(value).toBe('4');
    });

    // TODO: Test public methods and more possible user interactions
});

